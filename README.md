#ATLAS API integration
This project connects to ATLAS to query their data and display it on the front end.

It is built using two docker images for the services that it uses and has Xdebug enabled
for debugging.

## Getting setup
Clone the repository using
```git
git clone https://bitbucket.org/Kieranwilson/atlas-api.git
```
After this, run docker-compose up
```bash
docker-compose up
```
The setup process will create a new .env file that requires your atlas api key.
After putting in this key you will have to start the docker-container once more.
This time as part of the build process it will fill the tables with location data.

## Accessing the php machine
If you change the folder from chatbot then the name will have to be updated
```bash
docker exec -it atlasintegration_php_1 bash
```
to see a list of docker machines use
```bash
docker ps
```
to see the log history for the machine use
```bash
docker logs chatbot_php_1
```
Laravel logs will get written as normal

## Extras

### Xdebug Configuration
xDebug is using remote_host configuration to talk to the host machine, this means that the remote_host must be
set correctly for your IDE to receive the connection. To do this edit the .env in the project root, changing the
`XDEBUG_HOST_IP` to your local IP address. After this it requires your IDE to be configured.

#### PHPSTORM Xdebug
Go to File->Settings and then go to `Languages & Frameworks -> PHP -> Debug` and ensure the Xdebug port is set to 9000
(which is the default port), that it can accept extenal connections, also make sure that the zend debug port is not set 
to 9000 (9001 for example).

Now go into `Languages & Frameworks -> PHP -> Debug->DBGp Proxy` and ensure that the port is
also 9000 and the host is equal to your local ip address. Save all changes.

Upon forming an Xdebug connection if you go to the debug tab you should get a message saying  
```
Can't find a sourceposition. Server with name ''aravel' doesn't exist"
Configure Servers
```
Click configure servers, create a new one named laravel, set the host to localhost, port to 9000, debugger to Xdebug
and check use path mappings. Now map the src folder to `/var/www/html`
