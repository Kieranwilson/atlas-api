<?php

return [
		'api_key' => env('ATLAS_API_KEY'),
		'api_url' => env('ATLAS_URL', 'http://atlas.atdw-online.com.au/api/atlas/'),
		'cache_for' => env('REDIS_CACHE_FOR', 20),
		'retrieve_x_pages' => env('RETRIEVE_X_PAGES', 10)
];
