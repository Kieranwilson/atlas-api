#!/bin/bash -eu

printf "\u001b[31m\u001b[47mWorker Starting\033[0m\n"
php artisan queue:work  2> /dev/stderr > /dev/stdout &

while sleep 2
do
	ag -l .* --ignore-dir vendor/ --ignore-dir node_modules/ | \
	entr echo 'test' > ./temp.log
done
