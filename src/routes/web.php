<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/products', 'ProductController@getProducts');
Route::get('/getProduct/{id}', 'ProductController@getProduct');
Route::get('/getSimilar/{id}', 'ProductController@getSimilar');

Route::get('/getregions', 'LocationController@getRegions');
Route::get('/getareas', 'LocationController@getAreas');
Route::get('/getsuburbs', 'LocationController@getSuburbs');

Route::get('/{catchall?}', function () {
	// Route to redirect for Vue handling
	return view('atlasapi');
})->where('catchall', '(.*)');
