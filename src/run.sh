#!/bin/bash -eu

output()
{
	FOREGROUND='\u001b[31m'
	BACKGROUND='\u001b[47m'
	RESET='\033[0m'
	printf "${FOREGROUND}${BACKGROUND}$@${RESET}\n"
}

output "Updating composer"
composer install -vvv

output "Updating node packages"
npm install --verbose

if ! [ -f "./.env" ]
then
	output "Creating .env, don't forget to add your API key"
	composer run-script post-root-package-install
	composer run-script post-create-project-cmd
fi

output "Starting database migrations"
php artisan migrate

output "Refresh database"
php artisan atlas:sync

output "Starting script watcher"
npm run watch -- --watch-poll 2> /dev/stderr > /dev/stdout &

output "Apache launching"
source /etc/apache2/envvars
exec apache2 -D FOREGROUND
