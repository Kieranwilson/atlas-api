import Products from './components/Products.vue';
import Product from './components/Product.vue';

export const routes = [
	{ path: '/', component: Products },
	{ path: '/search', component: Products,  name: 'search'  },
	{ path: '/product/:id', component: Product, name: 'product' },
	{ path: '*', redirect: '/' } // TODO: Add 404 handler here
];
