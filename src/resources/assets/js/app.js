/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import App from './App.vue'
import { routes } from './routes';

Vue.use(VueResource);
Vue.use(VueRouter);

const router = new VueRouter({
	routes,
	mode: 'history'
});

Vue.http.options.root = process.env.MIX_APP_URL;
Vue.http.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 **/

const app = new Vue({
    el: '#app',
	router,
	render: h => h(App)
});
