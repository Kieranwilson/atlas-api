<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Atlas API Integration</title>
</head>
<body>

<div id="app">
	&nbsp;
</div>

<script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
