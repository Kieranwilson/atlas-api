<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
	protected $guarded = [];

	public function state()
	{
		return $this->belongsTo('App\State', 'id', 'state_id');
	}

	public function area() {
		return $this->hasMany('App\Area', 'region_id', 'id');
	}
}
