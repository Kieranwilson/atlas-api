<?php

namespace App\Http\Controllers;

use App\Area;
use App\Region;
use App\Suburb;
use Illuminate\Http\Request;


class LocationController extends Controller
{
    public function getRegions() {
		return Region::all();
	}

	public function getAreas(Request $request) {
    	return Area::where('region_id', $request->input('region'))->get();
	}

	public function getSuburbs(Request $request) {
		return Suburb::where('area_id', $request->input('area'))->get();
	}
}
