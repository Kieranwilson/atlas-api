<?php

namespace App\Http\Controllers;

use App\Http\Services\ProductService;
use App\Jobs\CacheFutureRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{
	/**
	 * This function takes the request from the front end and returns the collection
	 *
	 * @param Request $request
	 * @param ProductService $service
	 * @return \Illuminate\Support\Collection
	 */
	public function getProducts(Request $request, ProductService $service) {
		$payload = $request->input('payload');
		$products = Cache::get($payload, function () use ($service, $payload) {
			CacheFutureRequests::dispatch($payload);
			$data = (array) json_decode($payload);

			$products = $service->getProducts($data);
			Cache::put($payload, $products, config('atlas.cache_for'));
			return $products;
		});
		return $products;
	}

	/**
	 * Receives from the front end and returns the product
	 *
	 * @param $id
	 * @param ProductService $service
	 * @return \Illuminate\Support\Collection
	 */
	public function getProduct($id, ProductService $service) {
		$key = 'product_'.$id;
		$product = Cache::get($key, function () use ($service, $id, $key) {
			$product = $service->getProduct($id);

			Cache::put($key, $product, config('atlas.cache_for'));
			return $product;
		});
		return $product;
	}

	/**
	 * Receives from the front end and returns similar products
	 *
	 * @param $id
	 * @param ProductService $service
	 * @return \Illuminate\Support\Collection
	 */
	public function getSimilar($id, ProductService $service) {
		$key = 'similar_'.$id;
		$product = Cache::get($key, function () use ($service, $id, $key) {
			$product = $service->getSimilar($id);

			Cache::put($key, $product, config('atlas.cache_for'));
			return $product;
		});
		return $product;
	}
}
