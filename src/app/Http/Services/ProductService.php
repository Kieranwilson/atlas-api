<?php

namespace App\Http\Services;

use function array_merge;
use function array_push;
use function collect;
use GuzzleHttp\Client;


class ProductService
{
	protected $fieldList = [
			'product_id', // Retrieves the seven digit product id, concatenated with GUID identifying the product call session
			'product_name', // Retrieves the name of the product
			'product_description', // Retrieves the description of the product
			'product_image', // Retrieves the URL for the product image
			'product_classifications', // Retrieves the list of product classifications that are associated to the product
			'product_category', // Retrieves the product category the product is associated to eg. Event
			'landscape_image', // Retrieves the URL for the landscape-format image for this product. This will be the same URL as product_image unless that resolves to a portrait-format image. This field can be used by distributors who have their site formatting set up to only accept landscape images.
			'geo', // Retrieves the geo code location for the product
			'start_date', // For products in category Event, and Tours, the start date will indicate when it will begin
			'end_date', // For products in category Event, the end date indicates the last day of the event
			'rating_aaa', // The AAA star rating value for product. This field is specifically for Accommodation category products
			'rate_from', // Is the minimum price of product.
			'rate_to', // Is the maximum price of product.
			'address', // Retrieves the address of the product as a single line
			'trip_advisor', // Retrieves whether or not the product has a trip advisor review associated to it, as a 'true', or 'false', value.
			'txa_identifier', // A list of strings used to identify a product to V3's booking services. This field can be null if no such identifiers exist.
			'internet_points', // Retrieves details of internet access points (paid or free) close to the product. This field can have multiple values.
			'comms', // Retrieves details of phone (tollfree or paid), fax, mobile, email, url, booking url and wap for the product. Each type can have multiple values.
			'comms_ph', // Retrieves the phone number (tollfree or paid) for the product if it exists. This field can have multiple values.
			'comms_mb', // Retrieves the mobile number for the product if it exists. This field can have multiple values.
			'comms_fx', // Retrieves the fax number for the product if it exists. This field can have multiple values.
			'comms_burl', // Retrieves the booking url for the product if it exists. This field can have multiple values.
			'comms_url', // Retrieves the website url for the product if it exists. This field can have multiple values.
			'comms_wap', // Retrieves the WAP address for the product if it exists. This field can have multiple values.
			'comms_em', // Retrieves the email address for the product if it exists. This field can have multiple values.
			'journey_distance', // Retrieves the total length of a journey. This field is specifically for Journey category products
			'journey_unit', // Retrieves the units (kms or mtr) for the length of the journey. Use with journey_distance.
			'status', // Returns the status of the listing. Can be one of 3 states - ACTIVE, INACTIVE, EXPIRED. Please see delta for usage.
			'next_occurrence', // Retrieves the date of next occurrence of the event.
	];
	protected $availableSort = [
			'dist', // order by only the distance away from the specified latlong.
			'rnd', // order randombly
	];
	protected $apiKey;
	protected $apiUrl;


	public function __construct()
	{
		$this->apiKey = config('atlas.api_key');
		$this->apiUrl = config('atlas.api_url');
	}

	/**
	 * This function goes to atlas and gets the data, converting it back to utf-8 so that is can
	 * be json decoded
	 *
	 * @param string $endpoint
	 * @param array $data
	 * @return mixed
	 */
	public function makeRequest($endpoint, $data = [])
	{
		$client = new Client();

		$data['key'] = $this->apiKey;
		$data['out'] = 'json';

		$response = $client->get($this->apiUrl . $endpoint, ['query' => $data]);
		$content = $response->getBody()->getContents();

		return json_decode(
				mb_convert_encoding($content, 'UTF-8', 'UTF-16LE')
		);
	}

	/**
	 * Get a list of all the states from ATLAS
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function getStates()
	{
		$states = $this->makeRequest('states');
		return collect($states);
	}

	/**
	 * Get a list of all the locations from ATLAS
	 *
	 * @param array $data
	 * @return \Illuminate\Support\Collection
	 */
	public function getLocations($data = [])
	{
		$locations = $this->makeRequest('locations', $data);
		return collect($locations);
	}

	/**
	 * Get all the states from ATLAS
	 *
	 * @param array $opts
	 * @return \Illuminate\Support\Collection
	 */
	public function getProducts($opts = [])
	{
		$data = [
				'fl' => implode(',', $this->fieldList),
				'state' => 'NSW',
				'pge' => 1,
				'size' => 25
		];

		$data = array_merge($data, $opts);

		$products = $this->makeRequest('products', $data);
		return collect($products);
	}


	/**
	 * Get similar product listings
	 *
	 * @param $productId
	 * @param array $data
	 * @return \Illuminate\Support\Collection
	 */
	public function getSimilar($productId, $data = []) {
		$data['productid'] = $productId;

		// My Little Pony request
		$products = $this->makeRequest('mlp', $data);

		return collect($products);
	}

	/**
	 * Get the specific product from ATLAS
	 *
	 * @param $productId
	 * @return \Illuminate\Support\Collection
	 */
	public function getProduct($productId)
	{
		$data = [
			'productId' => $productId
		];

		$product = $this->makeRequest('product', $data);

		return collect($product);
	}

}
