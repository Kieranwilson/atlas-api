<?php

namespace App\Jobs;

use App\Http\Services\ProductService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;

class CacheFutureRequests implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $_payload;
    protected $_service;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $payload)
    {
		$this->_payload = $payload;
		$this->_service = new ProductService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		$data = (array) json_decode($this->_payload);
		try {
			$curPage = $data['pge'];
			for ($page = $curPage+1; $page <= $curPage + config('atlas.retrieve_x_pages'); $page++)
			{
				$data['pge'] = $page;
				$products = $this->_service->getProducts($data);
				Cache::put(json_encode($data), $products, config('atlas.cache_for'));
			}
		} catch (Exception $ex) {
			echo 'Reached end of page, should perform checks later';
		}
    }
}
