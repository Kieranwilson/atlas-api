<?php

namespace App\Console\Commands;

use App\Area;
use App\Http\Services\ProductService;
use App\Region;
use App\State;
use App\Suburb;
use Exception;
use Illuminate\Console\Command;
use function in_array;
use stdClass;

class AtlasSync extends Command
{
	private $enabledStates = ['NSW'];
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'atlas:sync';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sync the tables with atlas';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Retrieve the states from atlas and update the database
	 *
	 * @param $service ProductService
	 */
	private function saveStates($service)
	{
		$this->info('Retrieving states from Atlas');
		$states = $service->getStates();

		foreach ($states as $state) {
			$newState = State::find($state->StateId);
			if ( ! $newState ) {
				$this->info('Inserting new state: ' . $state->Name);
				$newState = new State(['id' => $state->StateId]);
			}
			if (in_array($state->Code, $this->enabledStates)) {
				$newState->active = true;
			}
			$newState->code = $state->Code;
			$newState->name = $state->Name;
			$newState->save();
		}
	}

	/**
	 * Retrieve the locations from atlas and update the database
	 *
	 * @param $service ProductService
	 */
	private function saveLocations($service)
	{
		$this->info('Retrieving Locations from Atlas');

		$activeStates = State::where('active', true)->get();

		foreach ($activeStates as $state) {
			$locations = $service->getLocations(['st' => $state->code]);

			$this->info('Processing ' . count($locations) . ' locations');
			$bar = $this->output->createProgressBar(count($locations));

			foreach ($locations as $location) {
				if (is_null($location->DomesticRegionId)) {
					// For TAS there were some suburbs not linked
					// to regions, don't actually need to handle that yet
					continue;
				}
				$newRegion = $this->handleRegion($location, $state);

				$newArea = $this->handleArea($location, $newRegion);

				$this->saveSuburbs($location->Suburbs, $newArea);

				$bar->advance();
			}
			$bar->finish();
			$this->line('');
		}
	}

	/**
	 * @param $suburbs stdClass
	 * @param $area Area
	 */
	public function saveSuburbs($suburbs, $area)
	{
		foreach ($suburbs->suburb as $suburb) {
			$newSuburb = Suburb::find($suburb->SuburbId);
			if ( ! $newSuburb ) {
				$newSuburb = new Suburb(['id' => $suburb->SuburbId]);
			}
			$newSuburb->name = $suburb->Name;
			$newSuburb->postcode = $suburb->PostCode;
			$newSuburb->area_id = $area->id;
			$newSuburb->save();
		}
	}

	/**
	 * @param $data stdClass
	 * @param $region Region
	 * @return Area
	 */
	private function handleArea($data, $region)
	{
		$newArea = Area::find($data->AreaId);
		if ( ! $newArea ) {
			$newArea = new Area(['id' => $data->AreaId]);
		}
		$newArea->name = $data->Name;
		$newArea->code = $data->Type;
		$newArea->type = $data->Code;
		$newArea->region_id = $region->id;
		$newArea->save();

		return $newArea;
	}

	/**
	 * @param $data stdClass
	 * @param $state Region
	 * @return Region
	 */
	private function handleRegion($data, $state)
	{

		$newRegion = Region::find($data->DomesticRegionId);
		if ( ! $newRegion ) {
			$newRegion = new Region(['id' => $data->DomesticRegionId]);
		}
		$newRegion->name = $data->DomesticRegionName;
		$newRegion->state_id = $state->id;

		$newRegion->save();

		return $newRegion;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->info('Syncing locations from Atlas');
		$service = new ProductService();

		$this->saveStates($service);
		$this->saveLocations($service);
	}
}
