<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	protected $guarded = [];

	public function region()
	{
		return $this->belongsTo('App\Region', 'id', 'region_id');
	}

	public function suburb() {
		return $this->hasMany('App\Suburb', 'area_id', 'id');
	}
}
