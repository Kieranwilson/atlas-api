<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	protected $guarded = [];

	public function region() {
		return $this->hasMany('App\Region', 'state_id', 'id');
	}
}
